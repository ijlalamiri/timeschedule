@if (session()->has('failed'))
    <div class="alert alert-danger }}">
        {{ session('failed') }}
        {{ session()->forget('failed') }}
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success }}">
        {{ session('success') }}
        {{ session()->forget('success') }}
    </div>
@endif
@if(count($errors) > 0)
    <div class="error">
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <strong>{{$error}}</strong><br>
            @endforeach
        </div>
    </div>
@endif
