@extends('layouts.app')
@section('content')
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @include('partials.alert_message')

                    <div class="card">
                        <div class="card-header">
                                <span class="float-left-start">
                                    <a href="{{ route("teacher.index") }}"><i class="fa fa-arrow-left"></i>Back</a>
                                </span>
                            <h3 class="text-center">{{ __("Add Teacher") }}</h3>
                        </div>
                    </div>
                    <div class="card-body bg-white py-5 form-block">
                        <div class="row">
                            <div class="col-8 mx-auto">
                                <form method="POST" action="{{ route('teacher.store') }}">
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-12 col-md-2 col-form-label">Name </label>
                                        <div class="col-12 col-md-8">
                                            <input class="form-control" type="text" name="name"
                                                   value="{{ old('name') }}" placeholder="Enter Name"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12 col-md-2 col-form-label">Qualification </label>
                                        <div class="col-12 col-md-8">
                                            <input class="form-control" type="text" name="qualification"
                                                   value="{{ old('qualification') }}"
                                                   placeholder="Enter Qualification"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12 col-md-2 col-form-label">Start Time </label>
                                        <div class="col-12 col-md-8">
                                            <input class="form-control" type="time" name="start_time"
                                                   value="{{ old('start_time') }}" placeholder="Enter Starting Time"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12 col-md-2 col-form-label">End Time </label>
                                        <div class="col-12 col-md-8">
                                            <input class="form-control" type="time" name="end_time"
                                                   value="{{ old('end_time') }}" placeholder="Enter Ending Time"/>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="text-center">
                                        <input type="submit" value="Create Teacher" class="btn btn-outline-primary"/>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
