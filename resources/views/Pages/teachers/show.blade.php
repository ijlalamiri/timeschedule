@extends('layouts.app')
@section('content')
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @include('partials.alert_message')

                    <div class="card">
                        <div class="row px-3">
                            <div class="col-12 py-4">
                                <h2 class="float-lg-start">Teacher Details</h2>
                            </div>
                            <div class="col-12 col-md-4">
                                {{ "Name :" .$time_table->name }}
                            </div>
                            <div class="col-12 col-md-4">
                                {{ "Qualification :" .$time_table->qualification }}
                            </div>
                            <div class="col-12 col-md-4">
                                {{ "Timing :" .\Carbon\Carbon::parse($time_table->start_time)->format('H:i') ."-". \Carbon\Carbon::parse($time_table->end_time)->format('H:i') }}
                            </div>
                        </div>
                        <div class="card-body bg-white">
                            <div class="row">
                                <div class="col-12 py-4">
                                    <h2 class="float-lg-start">Schedule:</h2>
                                </div>
                                <div class="col-12">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr class="table-lighter-blue">
                                            <th scope="col">#</th>
                                            <th scope="col">Class</th>
                                            <th scope="col">Subject</th>
                                            <th scope="col">Timing</th>
                                            <th scope="col">Duration</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($time_table)
                                            @foreach($time_table['time_schedule'] as $key=>$timeTable)
                                                <tr>
                                                    <td>
                                                        {{++$key}}
                                                    </td>
                                                    <td>
                                                        {{$timeTable->classes->name}}
                                                    </td>
                                                    <td>
                                                        {{$timeTable->subjects->name}}
                                                    </td>
                                                    <td>
                                                        {{\Carbon\Carbon::parse($timeTable->start_time)->format('H:i')}}  - {{\Carbon\Carbon::parse($timeTable->end_time)->format('H:i')}}
                                                    </td>

                                                    <td>
                                                        {{ \Carbon\Carbon::parse($timeTable->start_time)->diffInMinutes(\Carbon\Carbon::parse($timeTable->end_time)) }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
