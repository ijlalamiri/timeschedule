@extends('layouts.app')
@section('content')
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @include('partials.alert_message')

                    <div class="card">
                        <div class="card-header">
                                <span class="float-left-start">
                                    <a href="{{ route("time-table.index") }}"><i class="fa fa-arrow-left"></i>Back</a>
                                </span>
                            <h3 class="text-center">{{ __("Adding Period to Time Table") }}</h3>
                        </div>
                    </div>
                    <div class="card-body bg-white py-5 form-block">
                        <div class="row">
                            <div class="col-8 mx-auto">
                                <form method="POST" action="{{ route('time-table.store')}}">
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-12 col-md-2 col-form-label">Class </label>
                                        <div class="col-12 col-md-8">
                                            <select class="form-control" name="class_id">
                                                @if(isset($classes) && $classes)
                                                    <option value="">Select Class</option>
                                                    @foreach($classes as $key=>$class)
                                                        <option
                                                            @if(old('class_id') == $class->id) selected
                                                            @endif
                                                            value="{{ $class->id }}">{{ $class->name }}</option>
                                                    @endforeach
                                                @else
                                                    <option>Please Add Class !</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12 col-md-2 col-form-label">Subjects </label>
                                        <div class="col-12 col-md-8">
                                            <select class="form-control" name="subject_id">
                                                @if(isset($subjects) && $subjects)
                                                    <option value="">Select Subject</option>
                                                    @foreach($subjects as $key=>$subject)
                                                        <option
                                                            @if(old('subject_id') == $subject->id) selected
                                                            @endif
                                                            value="{{ $subject->id }}">{{ $subject->name }}</option>
                                                    @endforeach
                                                @else
                                                    <option>Please Add Subject !</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-12 col-md-2 col-form-label">Teachers </label>
                                        <div class="col-12 col-md-8">
                                            <select class="form-control" name="teacher_id">
                                                @if(isset($teachers) && $teachers)
                                                    <option value="">Select Teacher</option>
                                                    @foreach($teachers as $key=>$teacher)
                                                        <option
                                                            @if(old('teacher_id') == $teacher->id) selected
                                                            @endif
                                                            value="{{ $teacher->id }}">{{ $teacher->name }}</option>
                                                    @endforeach
                                                @else
                                                    <option>Please Add Teacher !</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12 col-md-2 col-form-label">Start Time </label>
                                        <div class="col-12 col-md-8">
                                            <input class="form-control" type="time" name="start_time"
                                                   value="{{ old('start_time') }}"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12 col-md-2 col-form-label">End Time </label>
                                        <div class="col-12 col-md-8">
                                            <input class="form-control" type="time" name="end_time"
                                                   value="{{ old('end_time') }}"/>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="text-center">
                                        <input type="submit" value="Create" class="btn btn-outline-primary"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
