@extends('layouts.app')
@section('content')
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @include('partials.alert_message')

                    <div class="card">
                        <div class="card-header">
                            <h2 class="float-lg-start">Time Table</h2><span>
                                 <a class="float-lg-end btn btn-primary"
                                    href="{{ route('time-table.create') }}">
                                ADD <i class="fa fa-plus fa-icon"></i>
                            </a>
                            </span>

                        </div>
                    </div>
                    <div class="card-body bg-white">
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr class="table-lighter-blue">
                                        <th scope="col">#</th>
                                        <th scope="col">Class Id</th>
                                        <th scope="col">Subject Id</th>
                                        <th scope="col">Teacher Id</th>
                                        <th scope="col">Timing</th>
                                        <th scope="col" colspan="2" class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if($timeTables)
                                        @foreach($timeTables as $key=>$timeTable)
                                            <tr>
                                                <td>
                                                    {{++$key}}
                                                </td>
                                                <td>
                                                    {{$timeTable->classes->name}}
                                                </td>
                                                <td>
                                                    {{$timeTable->subjects->name}}
                                                </td>
                                                <td>
                                                    {{$timeTable->teachers->name}}
                                                </td>
                                                <td>
                                                    {{$timeTable->start_time}} 'to' {{$timeTable->end_time}}
                                                </td>
                                                <td>
                                                    <a href="{{ route('time-table.edit',$timeTable->id) }}"
                                                    ><i class="fa fa-pencil-alt"></i> Edit</a>
                                                    <form class="d-inline-block"
                                                          action="{{ route('time-table.destroy',$timeTable->id) }}"
                                                          method="POST" class="d-block mb-0">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        <button type="submit"><i
                                                                class="fa fa-trash"></i> Delete
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

    </section>
@endsection
