@extends('layouts.app')
@section('content')
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @include('partials.alert_message')

                    <div class="card">
                        <div class="card-header">
                            <h2 class="float-lg-start">Subjects</h2><span>
                                 <a class="float-lg-end btn btn-primary"
                                    href="{{ route('subject.create') }}">
                                ADD <i class="fa fa-plus fa-icon"></i>
                            </a>
                            </span>

                        </div>
                    </div>
                    <div class="card-body bg-white">
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr class="table-lighter-blue">
                                        <th scope="col">#</th>
                                        <th scope="col">Subject</th>
                                        <th scope="col">Subject Code</th>
                                        <th scope="col">Class</th>
                                        <th scope="col" colspan="2" class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if($subjects)
                                        @foreach($subjects as $key=>$subject)
                                            <tr>
                                                <td>
                                                    {{++$key}}
                                                </td>
                                                <td>
                                                    {{$subject->name}}
                                                </td>
                                                <td>
                                                    {{$subject->code}}
                                                </td>
                                                <td>
                                                    {{$subject->classDetails->name}}
                                                </td>
                                                <td>
                                                    <a href="{{ route('subject.edit',$subject->id) }}"
                                                    ><i class="fa fa-pencil-alt"></i> Edit</a>
                                                    <form class="d-inline-block"
                                                          action="{{ route('subject.destroy',$subject->id) }}"
                                                          method="POST" class="d-block mb-0">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        <button type="submit"><i
                                                                class="fa fa-trash"></i> Delete
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

    </section>
@endsection
