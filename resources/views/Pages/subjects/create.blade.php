@extends('layouts.app')
@section('content')
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @include('partials.alert_message')

                    <div class="card">
                        <div class="card-header">
                                <span class="float-left-start">
                                    <a href="{{ route("subject.index") }}"><i class="fa fa-arrow-left"></i>Back</a>
                                </span>
                            <h3 class="text-center">{{ __("Add Subject") }}</h3>
                        </div>
                    </div>
                    <div class="card-body bg-white py-5 form-block">
                        <div class="row">
                            <div class="col-8 mx-auto">
                                <form method="POST" action="{{ route('subject.store') }}">
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-12 col-md-2 col-form-label">Name </label>
                                        <div class="col-12 col-md-8">
                                            <input class="form-control" type="text" name="name"
                                                   value="{{ old('name') }}" placeholder="Enter Name"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12 col-md-2 col-form-label">Code </label>
                                        <div class="col-12 col-md-8">
                                            <input class="form-control" type="text" name="code"
                                                   value="{{ old('code') }}"
                                                   placeholder="Enter Code"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12 col-md-2 col-form-label">Class </label>
                                        <div class="col-12 col-md-8">
                                            <select class="form-control" name="class_id">
                                                @if(isset($classes) && $classes)
                                                    <option value="">Select Class</option>
                                                    @foreach($classes as $key=>$class)
                                                        <option
                                                            @if(old('class_id') == $class->id) selected
                                                            @endif
                                                            value="{{ $class->id }}">{{ $class->name }}</option>
                                                    @endforeach
                                                @else
                                                    <option>Please Add Class !</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="text-center">
                                        <input type="submit" value="Create Subject" class="btn btn-outline-primary"/>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
