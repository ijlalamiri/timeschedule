<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TeachersController;
use \App\Http\Controllers\SubjectsController;
use \App\Http\Controllers\ClassesController;
use \App\Http\Controllers\TimeTablesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Auth.login');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::resource('teacher', TeachersController::class);
Route::resource('class', ClassesController::class);
Route::resource('subject', SubjectsController::class);
Route::resource('time-table', TimeTablesController::class);
