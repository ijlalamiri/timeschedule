<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectTableSeeder extends Seeder
{
    private $_tableName = "subjects";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // First empty the tables
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table($this->_tableName)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $plans = [
            [
                'id' => 1,
                'name' => "Physics",
                'code' =>"001",
                'class_id'=>1
            ],
            [
                'id' => 2,
                'name' => "Mathematics",
                'code' =>"002",
                'class_id'=>2
            ],
            [
                'id' => 3,
                'name' => "Chemistry",
                'code' =>"003",
                'class_id'=>3
            ],
            [
                'id' => 4,
                'name' => "Mathematics",
                'code' =>"001",
                'class_id'=>4
            ]
        ];

        DB::table($this->_tableName)->insert($plans);
    }
}
