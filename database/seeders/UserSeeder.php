<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    private $_tableName = "users";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // First empty the tables
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table($this->_tableName)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $plans = [
            [
                'id' => 1,
                'name' => "Admin",
                'email' => 'admin@admin.com',
                'password' => Hash::make('12345678')
            ]
        ];

        DB::table($this->_tableName)->insert($plans);
    }
}
