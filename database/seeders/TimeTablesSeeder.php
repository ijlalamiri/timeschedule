<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimeTablesSeeder extends Seeder
{
    private $_tableName = "time_tables";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // First empty the tables
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table($this->_tableName)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $plans = [
            [
                'id' => 1,
                'class_id' => 1,
                'subject_id' => 1,
                'teacher_id' =>1,
                'start_time' => '8:30',
                'end_time' => '09:15',
            ],
            [
                'id' => 2,
                'class_id' => 2,
                'subject_id' => 2,
                'teacher_id' =>1,
                'start_time' => '09:15',
                'end_time' => '10:00',
            ],
            [
                'id' => 3,
                'class_id' => 3,
                'subject_id' => 3,
                'teacher_id' =>1,
                'start_time' => '10:30',
                'end_time' => '11:15',
            ],
            [
                'id' => 4,
                'class_id' => 4,
                'subject_id' => 4,
                'teacher_id' =>1,
                'start_time' => '11:15',
                'end_time' => '12:00',
            ]
        ];

        DB::table($this->_tableName)->insert($plans);
    }
}
