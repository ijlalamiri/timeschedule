<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TeachersSeeder extends Seeder
{
    private $_tableName = "teachers";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // First empty the tables
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table($this->_tableName)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $plans = [
            [
                'id' => 1,
                'name' => "ABC",
                'Qualification' => 'MSC',
                'start_time' => '8:30',
                'end_time' => '12:30',
            ],
            [
                'id' => 2,
                'name' => "BCD",
                'Qualification' => 'MSC',
                'start_time' => '8:30',
                'end_time' => '12:30',
            ]
        ];

        DB::table($this->_tableName)->insert($plans);
    }
}
