<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ClassTableSeeder extends Seeder
{

    private $_tableName = "classes";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // First empty the tables
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table($this->_tableName)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $plans = [
            [
                'id' => 1,
                'name' => "VII",
            ],
            [
                'id' => 2,
                'name' => "VIII",
            ],
            [
                'id' => 3,
                'name' => "IX",
            ],
            [
                'id' => 4,
                'name' => "X",
            ]
        ];

        DB::table($this->_tableName)->insert($plans);
    }
}
