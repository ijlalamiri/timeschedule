<?php

namespace App\Models;

use App\Http\Controllers\TimeTablesController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teachers extends Model
{
    use HasFactory;
    public function time_schedule()
    {
        return $this->hasMany(TimeTables::class, 'teacher_id');
    }
}
