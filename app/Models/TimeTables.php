<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TimeTables extends Model
{
    use HasFactory;
    public function classes(){
        return $this->belongsTo(Classes::class, 'class_id');
    }
    public function subjects(){
        return $this->belongsTo(Subjects::class, 'subject_id');
    }
    public function teachers(){
        return $this->belongsTo(Teachers::class, 'teacher_id');
    }
}
