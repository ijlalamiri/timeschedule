<?php

namespace App\Http\Controllers;

use App\Http\Requests\TimeTableRequest;
use App\Models\Classes;
use App\Models\Subjects;
use App\Models\Teachers;
use App\Models\TimeTables;
use Illuminate\Http\Request;
use Ramsey\Uuid\Type\Time;

class TimeTablesController extends Controller
{
    private $_messages = [
        'CREATE_SUCCESS' => 'Period Added to Time Table Successfully.',
        'UPDATE_SUCCESS' => 'Period Updated to Time Table  Successfully.',
        'DELETE_SUCCESS' => 'Period Deleted from Time Table  Successfully.',
        'DELETE_FAILED' => 'Failed to delete period from Time Table.',
    ];

    public function index()
    {
        $timeTables = TimeTables::with('classes')
            ->with('subjects')
            ->with('teachers')
            ->get();
        return view('pages.timeTable.index', compact('timeTables'));

    }

    public function create()
    {
        $classes = Classes::all();
        $subjects =Subjects::all();
        $teachers = Teachers::all();
        return view('pages.timeTable.create',[
            'classes' => $classes,
            'subjects' => $subjects,
            'teachers' => $teachers
        ]);
    }

    public function store(TimeTableRequest $request)
    {
        $request->validated();
        $timeTable = new TimeTables();
        $timeTable->class_id = $request->class_id;
        $timeTable->subject_id = $request->subject_id;
        $timeTable->teacher_id = $request->teacher_id;
        $timeTable->start_time = $request->start_time;
        $timeTable->end_time = $request->end_time;
        $timeTable->save();
        return redirect()->route('time-table.index')->with('success', $this->_messages['CREATE_SUCCESS']);
    }

    public function show(TimeTables $timeTables)
    {
        //
    }

    public function edit(TimeTables $timeTable)
    {
        $classes = Classes::all();
        $subjects = Subjects::all();
        $teachers = Teachers::all();
        return view('pages.timeTable.edit', [
            'subjects' => $subjects,
            'classes' =>$classes,
            'teachers' =>$teachers,
            'timeTable' =>$timeTable
        ]);
    }

    public function update(TimeTableRequest $request, TimeTables $timeTable)
    {
        $request->validated();
        $timeTable->class_id = $request->class_id;
        $timeTable->subject_id = $request->subject_id;
        $timeTable->teacher_id = $request->teacher_id;
        $timeTable->start_time = $request->start_time;
        $timeTable->end_time = $request->end_time;
        $timeTable->save();
        return redirect()->route('time-table.index')->with('success', $this->_messages['UPDATE_SUCCESS']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TimeTables  $timeTables
     * @return \Illuminate\Http\Response
     */
    public function destroy(TimeTables $timeTable)
    {
        $timeTable->delete();
        return redirect()->route('time-table.index')->with('success', $this->_messages['DELETE_SUCCESS']);

    }
}
