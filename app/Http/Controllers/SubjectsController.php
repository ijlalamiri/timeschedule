<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubjectRequest;
use App\Models\Classes;
use App\Models\Subjects;
use App\Models\TimeTables;
use Illuminate\Http\Request;

class SubjectsController extends Controller
{
    private $_messages = [
        'CREATE_SUCCESS' => 'Subject Added Successfully.',
        'UPDATE_SUCCESS' => 'Subject Updated Successfully.',
        'DELETE_SUCCESS' => 'Subject Deleted Successfully.',
        'DELETE_CONSTRAINT' =>"Failed to Delete Subject already in Use",
        'DELETE_FAILED' => 'Failed to delete.',
    ];

    public function index()
    {
        $subjects = Subjects::all();
        return view('pages.subjects.index', compact('subjects'));
    }

    public function create()
    {
        $classes = Classes::all();
        return view('pages.subjects.create',compact('classes'));
    }

    public function store(SubjectRequest $request)
    {
        $request->validated();
        $subject = new Subjects();
        $subject->name = $request->name;
        $subject->code = $request->code;
        $subject->class_id = $request->class_id;
        $subject->save();
        return redirect()->route('subject.index')->with('success', $this->_messages['CREATE_SUCCESS']);
    }

    public function show(Subjects $subject)
    {
        //
    }

    public function edit(Subjects $subject)
    {
        $classes = Classes::all();
        return view('pages.subjects.edit', [
            'subject' => $subject,
            'classes' =>$classes
        ]);
    }

    public function update(SubjectRequest $request, Subjects $subject)
    {
        $request->validated();
        $subject->name = $request->name;
        $subject->code = $request->code;
        $subject->class_id = $request->class_id;
        $subject->save();
        return redirect()->route('subject.index')->with('success', $this->_messages['UPDATE_SUCCESS']);
    }

    public function destroy(Subjects $subject)
    {
        $timeTable = TimeTables::where('subject_id', $subject->id);
        if ($timeTable) {
            return redirect()->route('time-table.index')->with('failed', $this->_messages['DELETE_CONSTRAINT']);
        }
        $subject->delete();
        return redirect()->route('subject.index')->with('success', $this->_messages['DELETE_SUCCESS']);
    }
}
