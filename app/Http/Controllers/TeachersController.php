<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTeacherRequest;
use App\Models\Teachers;
use App\Models\TimeTables;
use Illuminate\Http\Request;
use Ramsey\Uuid\Type\Time;

class TeachersController extends Controller
{
    private $_messages = [
        'CREATE_SUCCESS' => 'Teacher Added Successfully.',
        'UPDATE_SUCCESS' => 'Teacher Updated Successfully.',
        'DELETE_SUCCESS' => 'Teacher Deleted Successfully.',
        'DELETE_CONSTRAINT' =>"Failed to Delete Teacher already in Use",
        'DELETE_FAILED' => 'Failed to delete.',
    ];

    public function index()
    {
        $teachers = Teachers::all();

        return view('pages.teachers.index', compact('teachers'));
    }

    public function create()
    {
        return view('pages.teachers.create');
    }

    public function store(CreateTeacherRequest $request)
    {
        $request->validated();
        $teacher = new Teachers();
        $teacher->name = $request->name;
        $teacher->qualification = $request->qualification;
        $teacher->start_time = $request->start_time;
        $teacher->end_time = $request->end_time;
        $teacher->save();
        return redirect()->route('teacher.index')->with('success', $this->_messages['CREATE_SUCCESS']);

    }

    public function show(Teachers $teacher)
    {
        //
        $time_table = Teachers::with('time_schedule')->where('id', '=',$teacher->id)->get()->first();
        return view('pages.teachers.show', [
            'time_table' => $time_table
        ]);
    }

    public function edit(Teachers $teacher)
    {
        return view('pages.teachers.edit', [
            'teacher' => $teacher
        ]);
    }

    public function update(CreateTeacherRequest $request, Teachers $teacher)
    {
        // Validation
        $request->validated();
        $teacher->name = $request->name;
        $teacher->qualification = $request->qualification;
        $teacher->start_time = $request->start_time;
        $teacher->end_time = $request->end_time;
        $teacher->save();
        return redirect()->route('teacher.index')->with('success', $this->_messages['UPDATE_SUCCESS']);

    }

    public function destroy(Teachers $teacher)
    {
        $timeTable = TimeTables::where('class_id', $teacher->id);
        if ($timeTable) {
            return redirect()->route('time-table.index')->with('failed', $this->_messages['DELETE_CONSTRAINT']);
        }
        $teacher->delete();
        return redirect()->route('teacher.index')->with('success', $this->_messages['DELETE_SUCCESS']);
        //
    }
}
