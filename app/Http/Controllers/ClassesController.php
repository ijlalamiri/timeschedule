<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClassRequest;
use App\Models\Classes;
use App\Models\Subjects;
use App\Models\TimeTables;
use Illuminate\Http\Request;

class ClassesController extends Controller
{
    private $_messages = [
        'CREATE_SUCCESS' => 'Class Added Successfully.',
        'UPDATE_SUCCESS' => 'Class Updated Successfully.',
        'DELETE_SUCCESS' => 'Class Deleted Successfully.',
        'DELETE_FAILED' => 'Failed to delete.',
        'DELETE_CONSTRAINT' =>"Failed to Delete Class already in Use"
    ];

    public function index()
    {
        $classes = Classes::all();
        return view('pages.classes.index', compact('classes'));
    }

    public function create()
    {
        return view('pages.classes.create');
    }

    public function store(ClassRequest $request)
    {
        $request->validated();
        $teacher = new Classes();
        $teacher->name = $request->name;
        $teacher->save();
        return redirect()->route('class.index')->with('success', $this->_messages['CREATE_SUCCESS']);
    }

    public function show(Classes $classes)
    {
        //
    }

    public function edit(Classes $class)
    {
        return view('pages.classes.edit', [
            'class' => $class
        ]);
    }

    public function update(ClassRequest $request, Classes $classes)
    {
        $request->validated();
        $classes->name = $request->name;
        $classes->save();
        return redirect()->route('class.index')->with('success', $this->_messages['UPDATE_SUCCESS']);
    }

    public function destroy(Classes $class)
    {
        $subject = Subjects::where('class_id', $class->id);
        if ($subject) {
            return redirect()->route('class.index')->with('failed', $this->_messages['DELETE_CONSTRAINT']);
        }
        $timeTable = TimeTables::where('class_id', $class->id);
        if ($timeTable) {
            return redirect()->route('time-table.index')->with('failed', $this->_messages['DELETE_CONSTRAINT']);
        }
        $class->delete();
        return redirect()->route('class.index')->with('success', $this->_messages['DELETE_SUCCESS']);
    }
}
